import validator from "validator";

export const stringIsValid = (text) =>{
    return text.length > 1;
}

export const emailIsValid = (email) => {
    return validator.isEmail(email);
}

export const startDateIsValid = (startDate) => {
    return startDate !== null && startDate instanceof Date;
};

export const endDateIsValid = (endDate, startDate) => {
    return endDate instanceof Date && endDate >= startDate;
};

export const numSiretIsValid = (numSiret) => {
    return /^\d{14}$/.test(numSiret);
}

export const adressIsValid = (address) => {
    return address.length > 6;
}

export const postalCodeIsValid = (postalCode) => {
    return /^\d{5}$/.test(postalCode);
}

export const numTelIsValid = (numTel) => {
    return /^[0-9]{4,12}$/.test(numTel);
}

const pswHasEnoughCharacters = (password) => {
    return password.length > 5;
};
const pswHasEnoughNumbers = (password) => {
    return /\d/.test(password);
};
const pswHasEnoughLowercase = (password) => {
    return /[a-z]/.test(password);
};
const pswHasEnoughUppercase = (password) => {
    return /[A-Z]/.test(password);
};
const pswHasSpecialChar = (password) => {
    return /[-+_!@#$%^&*.,?]/.test(password);
};

export const passwordIsValid = (password) => {
    return (
        pswHasEnoughCharacters(password) &&
        pswHasEnoughNumbers(password) &&
        pswHasEnoughLowercase(password) &&
        pswHasEnoughUppercase(password) &&
        pswHasSpecialChar(password)
    );
}

export const passwordHelperText = (password) => {
    let res = "";
    if (!pswHasEnoughCharacters(password)) {
        res += "Le mot de passe doit contenir au moins 8 caractères.\n";
    }
    if (!pswHasEnoughNumbers(password)) {
        res += "Le mot de passe doit contenir au moins un chiffre.\n";
    }
    if (!pswHasEnoughLowercase(password)) {
        res +=
            "Le mot de passe doit contenir au moins une lettre minuscule.\n";
    }
    if (!pswHasEnoughUppercase(password)) {
        res +=
            "Le mot de passe doit contenir au moins une lettre majuscule.\n";
    }
    if (!pswHasSpecialChar(password)) {
        res +=
            "Le mot de passe doit contenir au moins un caractère spécial.\n";
    }
    return res;
};
