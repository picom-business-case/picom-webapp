// used everywhere else
const AccountType = {
    NOT_CONNECTED: null,
    CUSTOMER: "ROLE_CUSTOMER",
    ADMIN: "ROLE_ADMIN",
};

export default AccountType;
