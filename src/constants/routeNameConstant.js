export const ROUTE_HOME = "/";

export const ROUTE_LOGIN = "/connexion";

export const ROUTE_REGISTER = "/inscription";

export const ROUTE_LOGOUT = "/logout";

export const ROUTE_ACCOUNT_AD_LIST = "/compte/campagne-publicitaire";

export const ROUTE_ACCOUNT_CREATE_AD = "/compte/cree/campagne-publicitaire";

export const ROUTE_ACCOUNT_SETTINGS = "/compte/parametre";