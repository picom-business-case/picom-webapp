/**
 * Where every action type is.
 *
 * Action type example:
 *
 * export const ADD_TODO = "ADD_TODO";
 */
export const SET_ROLE = "SET_ROLE";

export const SET_USER_INFORMATIONS = "SET_USER_INFORMATIONS";
export const SET_USER_PROFILE_INFORMATIONS = "SET_USER_PROFILE_INFORMATIONS";

export const SET_USER_TOKEN = "SET_USER_TOKEN";
export const SET_USER_DISCONNECTED = "SET_USER_DISCONNECTED";

export const SET_USER_FAVOURITE = "SET_USER_FAVOURITE";