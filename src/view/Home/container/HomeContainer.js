import React, {useEffect} from "react";
import baseApi from "../../../services/baseApi";
import Services from "../../../components/Sections/Services";
import Projects from "../../../components/Sections/Projects";
import Blog from "../../../components/Sections/Blog";
import Pricing from "../../../components/Sections/Pricing";
import Contact from "../../../components/Sections/Contact";
import Header from "../../../components/Sections/Header";

const HomeContainer = () => {

    useEffect(() => {
        baseApi.get(`api/stop`).then((data) => {
            console.log(data);
        })
    })

    return (
        <>
            <Header />
            <Services />
            <Projects />
            <Blog />
            <Pricing />
            <Contact />
        </>
    )
}

export default HomeContainer;