import React, {useEffect, useState} from "react";
import {Container, Grid, Typography} from "@mui/material";
import AdItem from "../../../components/ads/AdItem";
import getAdsByUserAPI from "../../../services/Ad/getAdsByUserAPI";

const ListUserAdContainer = () => {
    const [adUserList, setAdUserList] = useState({});

    useEffect(() => {
        getAdsByUserAPI()
            .then((data) => {
                console.log("Received data : ", data);
                setAdUserList(data);
            } )
    }, [])

    console.log("User List", adUserList);
  return (
      <Container>
          <h1 className="extraBold font60 textCenter">Mes Campagnes publicitaires</h1>
          <Grid container mt={4}>
              <Grid item xs={12} md={8}>
                  <AdItem />
              </Grid>
          </Grid>

      </Container>
  )
}

export default ListUserAdContainer;