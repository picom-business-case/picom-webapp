import {
    ROUTE_ACCOUNT_AD_LIST,
    ROUTE_HOME,
    ROUTE_LOGIN,
    ROUTE_LOGOUT,
    ROUTE_REGISTER
} from "../constants/routeNameConstant";
import Home from "../view/Home";
import Login from "../components/login/Login";
import Register from "../components/register/Register";
import {LogoutFeature} from "../components/logout/Logout";
import {ListUserAd} from "../view/ListUserAd";
import AccountType from "../constants/AccountType";

const routing = [
    {
        path: ROUTE_HOME,
        component: <Home />
    },
    {
        path: ROUTE_LOGIN,
        component: <Login />
    },
    {
        path: ROUTE_REGISTER,
        component: <Register />
    },
    {
        path: ROUTE_LOGOUT,
        component: <LogoutFeature />
    }
];
 const routingConnected= [
     {
         path: ROUTE_ACCOUNT_AD_LIST,
         component: <ListUserAd />,
         isAccessibleFor: [
             AccountType.CUSTOMER,
             AccountType.ADMIN,
         ],

     },

 ];

 export default routing;
 export {routing, routingConnected};