import { routing, routingConnected } from "./routing";

const config = {
    routes: routing,
    routesConnected: routingConnected,
};

export default config;
