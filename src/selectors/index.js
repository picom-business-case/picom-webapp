//Select the state of the Role stored in the Redux Store
export const RoleSelector = ({ user }) => user.role;

//Select the User stored in the Redux Store
export const UserInformationsSelector = ({ user }) => user;
