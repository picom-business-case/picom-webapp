import React from "react";
import {Container, Grid, Typography} from "@mui/material";
import Box from "@mui/material/Box";
import theme from "../../theme";
import { useNavigate  } from "react-router-dom";
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import InstagramIcon from '@mui/icons-material/Instagram';
import FacebookIcon from '@mui/icons-material/Facebook';

const FooterCustomer = () => {

    const navigate = useNavigate();

    const redirectToLegalApprovement = () => {
      navigate("/mentions-légales");
    }

    const redirectToContact = () => {
      navigate("Contact");
    }

    return(
        <Box  sx={{
                    backgroundColor: theme.palette.primary.main,
                }}
        >
            <Container sx={{
                    backgroundColor: theme.palette.primary.main,
                    height: "150px"
                }}
            >
                <Grid container sx={{height: "100%"}}>
                    <Grid item container xs={12} md={6} justifyContent="center" alignItems="center">
                        <Typography
                            paragraph
                            sx={{
                                color: "white",
                                mr: 3,
                            }}
                        >
                            2019 Ⓒ Picom {"  "}
                        </Typography>
                        <Typography
                            paragraph
                            onClick={redirectToLegalApprovement}
                            sx={{
                                '&hover':{
                                    cursor: "pointer",
                                },
                                color: "white",
                                mr: 3,
                            }}
                        >
                            Mention Légales {" "}
                        </Typography>
                        <Typography
                            paragraph
                            onClick={redirectToContact}
                            sx={{
                                '&hover':{
                                    cursor: "pointer",
                                },
                                color: "white",
                                mr: 3,
                            }}
                        >
                            Contact {" "}
                        </Typography>
                    </Grid>
                    <Grid item container xs={12} md={6} alignItems="center" justifyContent="center">
                        <FacebookIcon sx={{fontSize: "80px", color: "white"}}/>
                        <LinkedInIcon sx={{fontSize: "80px", color: "white"}} />
                        <InstagramIcon sx={{fontSize: "80px", color: "white"}}/>
                    </Grid>
                </Grid>
            </Container>
        </Box>
    )
}

export default FooterCustomer