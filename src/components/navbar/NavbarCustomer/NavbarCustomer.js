import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import AdbIcon from '@mui/icons-material/Adb';
import logoPicom from "../../../assets/logo/logoPiCom.png";
import AddBoxOutlinedIcon from '@mui/icons-material/AddBoxOutlined';
import LoginOutlinedIcon from '@mui/icons-material/LoginOutlined';
import LogoutOutlinedIcon from '@mui/icons-material/LogoutOutlined';
import FolderOpenOutlinedIcon from '@mui/icons-material/FolderOpenOutlined';
import PersonAddAltOutlinedIcon from '@mui/icons-material/PersonAddAltOutlined';
import {Grid} from "@mui/material";
import {useNavigate} from "react-router-dom";
import theme from "../../../theme";
import {ROUTE_LOGIN, ROUTE_LOGOUT, ROUTE_REGISTER} from "../../../constants/routeNameConstant";



const pages = [
    {
        name:'Inscription',
        icon: <PersonAddAltOutlinedIcon fontSize="large" />,
        link: ROUTE_REGISTER
    },
    {
        name:'Connexion',
        icon: <LoginOutlinedIcon fontSize="large"/>,
        link: ROUTE_LOGIN
    }];
const pagesConnected = [
        {
            name:'Nouvelle annonce',
            icon: <AddBoxOutlinedIcon fontSize="large" />,
            link: '/compte/ajout-annonce'
        },
        {
            name:'Mes annonces',
            icon: <FolderOpenOutlinedIcon fontSize="large" />,
            link: '/compte/annonce'
        },
        {
            name:'Compte',
            icon: <AdbIcon fontSize="large" />,
            link: '/compte/parametres'
        },
        {
            name:'Déconnexion',
            icon: <LogoutOutlinedIcon fontSize="large" />,
            link: ROUTE_LOGOUT
        }
    ];
//const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];

const NavbarCustomer = ({isConnected = false}) => {

    const navigate = useNavigate();
    const [anchorElNav, setAnchorElNav] = React.useState(null);
  //  const [anchorElUser, setAnchorElUser] = React.useState(null);

    const handleOpenNavMenu = (event) => {
        setAnchorElNav(event.currentTarget);
    };
   /* const handleOpenUserMenu = (event) => {
        setAnchorElUser(event.currentTarget);
    };*/

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

   /* const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };*/

    return (
        <AppBar position="static" color="transparent">
            <Container maxWidth="lg">
                <Toolbar disableGutters>
                    {/*<Typography
                        variant="h6"
                        noWrap
                        component=""
                        href="/"
                        sx={{
                            mr: 2,
                            display: { xs: 'none', md: 'flex' },
                            fontFamily: 'monospace',
                            fontWeight: 700,
                            letterSpacing: '.3rem',
                            color: 'inherit',
                            textDecoration: 'none',
                        }}
                    >
                        LOGO
                    </Typography>*/}
                    <Box
                        sx={{
                            mr: 2,
                            display: { xs: 'none', md: 'flex' },
                        }}
                    >
                        <img
                            src={logoPicom}
                            alt="picom"
                            style={{height: "50px"}}
                            onClick={() => navigate('/')}
                        />
                    </Box>

                    <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleOpenNavMenu}
                            color="inherit"
                        >
                            <MenuIcon />
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorElNav}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'left',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'left',
                            }}
                            open={Boolean(anchorElNav)}
                            onClose={handleCloseNavMenu}
                            sx={{
                                display: { xs: 'block', md: 'none' },
                            }}
                        >
                            {isConnected ?
                                pagesConnected.map((page) => (
                                        <MenuItem key={page.name} onClick={() => {handleCloseNavMenu(); navigate(page.link) }}>
                                            <Typography textAlign="center">{page.name}</Typography>
                                        </MenuItem>
                                    ))
                                :
                                pages.map((page) => (
                                        <MenuItem key={page.name} onClick={() => {handleCloseNavMenu(); navigate(page.link) }}>
                                            <Typography textAlign="center">{page.name}</Typography>
                                        </MenuItem>
                                    ))
                            }
                        </Menu>
                    </Box>
                    <Typography
                        variant="h5"
                        noWrap
                        component="a"
                        href=""
                        sx={{
                            mr: 2,
                            display: { xs: 'flex', md: 'none' },
                            flexGrow: 1,
                            fontFamily: 'monospace',
                            fontWeight: 700,
                            letterSpacing: '.3rem',
                            color: 'inherit',
                            textDecoration: 'none',
                        }}
                    >
                        <img src={logoPicom} alt="picom" style={{height: "50px"}}/>
                    </Typography>
                    <Grid container sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
                        {
                            isConnected ?

                                    pagesConnected.map((page, index) => (


                                            index === 0 ? (
                                                <Grid
                                                    container
                                                    item
                                                    xs={3}
                                                    alignItems="center"
                                                    justifyContent="center"
                                                    key={page.name}
                                                >
                                                    <Button
                                                        variant="contained"
                                                        startIcon={page.icon}
                                                        sx={{
                                                            height: "40px",
                                                        }}
                                                        onClick={() => navigate(page.link)}
                                                    >
                                                        {page.name}
                                                    </Button>
                                                </Grid>
                                            ) : (
                                                <Grid
                                                    container
                                                    item
                                                    alignItems="center"
                                                    justifyContent="center"
                                                    flexDirection="column"
                                                    xs={2}
                                                    key={page.name}
                                                    onClick={() => navigate(page.link)}
                                                    sx={{
                                                        color: theme.palette.primary.main,
                                                        '&:hover': {
                                                            cursor: "pointer",
                                                            color: theme.palette.primary.dark
                                                        }
                                                    }}
                                                >
                                                    {page.icon}
                                                    <Typography variant="p">{page.name}</Typography>
                                                </Grid>
                                            )
                                    ))

                                : pages.map((page, index) => (


                                            index === 0 ? (
                                                    <Grid
                                                        container
                                                        item
                                                        xs={3}
                                                        alignItems="center"
                                                        justifyContent="center"
                                                        marginRight={3}
                                                        key={page.name}
                                                    >
                                                        <Button
                                                            variant="contained"
                                                            startIcon={page.icon}
                                                            sx={{
                                                                height: "40px",
                                                            }}
                                                            onClick={() => navigate(page.link)}
                                                        >
                                                            {page.name}
                                                        </Button>
                                                    </Grid>
                                            ) : (
                                                <Grid
                                                    container
                                                    item
                                                    xs={2}
                                                    alignItems="center"
                                                    justifyContent="center"
                                                    flexDirection="column"
                                                    marginRight={3}
                                                    key={page.name}
                                                    onClick={() => navigate(page.link)}
                                                    sx={{
                                                        color: theme.palette.primary.main,
                                                        '&:hover': {
                                                            cursor: "pointer",
                                                            color: theme.palette.primary.dark
                                                        }
                                                    }}
                                                >
                                                    {page.icon}
                                                    <Typography variant="p">{page.name}</Typography>
                                                </Grid>
                                            )
                                    ))
                        }

                    </Grid>

                   {/* <Box sx={{ flexGrow: 0 }}>
                        <Tooltip title="Open settings">
                            <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                                <Avatar alt="Remy Sharp" src="/static/images/avatar/2.jpg" />
                            </IconButton>
                        </Tooltip>
                        <Menu
                            sx={{ mt: '45px' }}
                            id="menu-appbar"
                            anchorEl={anchorElUser}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={Boolean(anchorElUser)}
                            onClose={handleCloseUserMenu}
                        >
                            {settings.map((setting) => (
                                <MenuItem key={setting} onClick={handleCloseUserMenu}>
                                    <Typography textAlign="center">{setting}</Typography>
                                </MenuItem>
                            ))}
                        </Menu>
                    </Box>*/}
                </Toolbar>
            </Container>
        </AppBar>
    );
}

export default NavbarCustomer;