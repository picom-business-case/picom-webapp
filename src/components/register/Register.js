import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import {useNavigate} from "react-router-dom";
import registerAPI from "../../services/authentification/registerAPI";
import {useState} from "react";
import {
    adressIsValid,
    emailIsValid,
    numSiretIsValid, numTelIsValid,
    passwordHelperText,
    passwordIsValid, postalCodeIsValid,
    stringIsValid
} from "../../utils/FormValidator";
import AuthenticationService from "../../services/AuthenticationService";

function Copyright(props) {
    return (
        <Typography variant="body2" color="text.secondary" align="center" {...props}>
            {'Copyright © '}
            <Link color="inherit" href="https://mui.com/">
                Your Website
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

export default function Register() {

    const navigate = useNavigate();
    const [readyToSubmit, setReadyToSubmit] = useState(false);
    const [stockError, setStockError] = useState({
        firstname: {
            value: "",
            error: false,
        },
        lastname: {
            value: "",
            error: false,
        },
        email: {
            value: "",
            error: false,
        },
        password: {
            value: "",
            error: false,
        },
        phoneNumber: {
            value: "",
            error: false,
        },
        numSiret: {
            value: "",
            error: false,
        },
        companyName: {
            value: "",
            error: false,
        },
        roadName: {
            value: "",
            error: false,
        },
        postalCode: {
            value: "",
            error: false,
        },
        city: {
            value: "",
            error: false,
        },
        newsletter: {
            value: false,
            error: false
        }

    });

    const checkIfFormIsValid = () => {
        return stringIsValid(stockError.firstname.value) && stringIsValid(stockError.lastname.value) &&
            emailIsValid(stockError.email.value) && numTelIsValid(stockError.phoneNumber.value) &&
            numSiretIsValid(stockError.numSiret.value) && stringIsValid(stockError.companyName.value) &&
            adressIsValid(stockError.roadName.value) && postalCodeIsValid(stockError.postalCode.value) &&
            stringIsValid(stockError.city.value);
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        console.log(event.currentTarget);
        const data = {
            firstName : stockError.firstname.value,
            lastName: stockError.lastname.value,
            email: stockError.email.value,
            password: stockError.password.value,
            phoneNumber : stockError.phoneNumber.value,
            numSiret: stockError.numSiret.value,
            companyName: stockError.companyName.value,
            roadName: stockError.roadName.value,
            postalCode: stockError.postalCode.value,
            city: {
                name: stockError.city.value
            }
        }

        registerAPI(data)
            .then((user) => {
                console.log("Youkou good ", user);
                AuthenticationService
                    .executeJwtAuthenticationService(data.email, data.password)
                    .then((response) => {
                        AuthenticationService.registerSuccessfulLoginForJwt( response.data.email, response.data.accessToken);
                        console.log(response.data.accessToken);
                        navigate(`/compte`);
                    }).catch(() => {
                    /* this.setState({ showSuccessMessage: false })
                     this.setState({ hasLoginFailed: true })*/
                })
        })
    };


    const handleChange = (e) => {
        console.log(e.target.name)
        const name = e.target.name;

        if (name === "firstName"){
            if (!stringIsValid(e.target.value)){
                setStockError({...stockError, firstname: { value: e.target.value, error: true}});
            }else{
                setStockError({...stockError, firstname: { value: e.target.value, error: false}});
            }
        }

        if (name === "lastName"){
            if (!stringIsValid(e.target.value)){
                setStockError({...stockError, lastname: { value: e.target.value, error: true}});
            }else{
                setStockError({...stockError, lastname: { value: e.target.value, error: false}});
            }
        }

        if (name === "email"){
            if (!emailIsValid(e.target.value)){
                setStockError({...stockError, email: { value: e.target.value, error: true}});
            }else{
                setStockError({...stockError, email: { value: e.target.value, error: false}});
            }
        }

        if (name === "password"){
            if (!passwordIsValid(e.target.value)){
                setStockError({...stockError, password: { value: e.target.value, error: true}});
            }else{
                setStockError({...stockError, password: { value: e.target.value, error: false}});
            }
        }

        if (name === "phoneNumber"){
            if (!numTelIsValid(e.target.value)){
                setStockError({...stockError, phoneNumber: { value: e.target.value, error: true}});
            }else{
                setStockError({...stockError, phoneNumber: { value: e.target.value, error: false}});
            }
        }

        if (name === "numSiret"){
            if (!numSiretIsValid(e.target.value)){
                setStockError({...stockError, numSiret: { value: e.target.value, error: true}});
            }else{
                setStockError({...stockError, numSiret: { value: e.target.value, error: false}});
            }
        }

        if (name === "companyName"){
            if (!stringIsValid(e.target.value)){
                setStockError({...stockError, companyName: { value: e.target.value, error: true}});
            }else{
                setStockError({...stockError, companyName: { value: e.target.value, error: false}});
            }
        }

        if (name === "roadName"){
            if (!adressIsValid(e.target.value)){
                setStockError({...stockError, roadName: { value: e.target.value, error: true}});
            }else{
                setStockError({...stockError, roadName: { value: e.target.value, error: false}});
            }
        }

        if (name === "postalCode"){
            if (!postalCodeIsValid(e.target.value)){
                setStockError({...stockError, postalCode: { value: e.target.value, error: true}});
            }else{
                setStockError({...stockError, postalCode: { value: e.target.value, error: false}});
            }
        }

        if (name === "city"){
            if (!stringIsValid(e.target.value)){
                setStockError({...stockError, city: { value: e.target.value, error: true}});
            }else{
                setStockError({...stockError, city: { value: e.target.value, error: false}});
            }
        }
        setReadyToSubmit(checkIfFormIsValid());
    }



    return (
            <Container component="main" maxWidth="md">
                <CssBaseline/>
                <Box
                    sx={{
                        marginTop: 12,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Avatar sx={{m: 1, bgcolor: 'secondary.main'}}>
                        <LockOutlinedIcon/>
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Inscription
                    </Typography>
                    <Box component="form" noValidate onSubmit={handleSubmit} sx={{mt: 3}}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    autoComplete="given-name"
                                    name="firstName"
                                    required
                                    fullWidth
                                    id="firstName"
                                    label="Prénom"
                                    autoFocus
                                    onChange={handleChange}
                                    error={stockError.firstname.error}
                                    helperText={stockError.firstname.error &&
                                        "Veuillez saisir votre prénom"
                                    }
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    required
                                    fullWidth
                                    id="lastName"
                                    label="Nom"
                                    name="lastName"
                                    autoComplete="family-name"
                                    onChange={handleChange}
                                    error={stockError.lastname.error}
                                    helperText={stockError.lastname.error &&
                                        "Veuillez saisir votre nom"
                                    }
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    id="email"
                                    label="Email"
                                    name="email"
                                    autoComplete="email"
                                    type="email"
                                    onChange={handleChange}
                                    error={stockError.email.error}
                                    helperText={stockError.email.error &&
                                        "Veuillez saisir un email valide"
                                    }
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    name="password"
                                    label="Mot de passe"
                                    type="password"
                                    id="password"
                                    autoComplete="new-password"
                                    onChange={handleChange}
                                    error={stockError.password.error}
                                    helperText={stockError.password.error &&
                                        passwordHelperText(stockError.password.value)
                                    }
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    name="phoneNumber"
                                    label="Numéro de téléphone"
                                    type="tel"
                                    id="phoneNumber"
                                    autoComplete="phone-number"
                                    onChange={handleChange}
                                    error={stockError.phoneNumber.error}
                                    helperText={stockError.phoneNumber.error &&
                                        "Veuillez saisir un numéro de téléphone valide"
                                    }
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    autoComplete="given-name"
                                    name="numSiret"
                                    required
                                    fullWidth
                                    id="numSiret"
                                    label="Votre numéro SIRET"
                                    autoFocus
                                    onChange={handleChange}
                                    error={stockError.numSiret.error}
                                    helperText={stockError.numSiret.error &&
                                        "Veuillez saisir un numéro de téléphone valide"
                                    }
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    required
                                    fullWidth
                                    id="companyName"
                                    label="Nom de votre entreprise"
                                    name="companyName"
                                    autoComplete="company-name"
                                    onChange={handleChange}
                                    error={stockError.companyName.error}
                                    helperText={stockError.companyName.error &&
                                        "Veuillez saisir un numéro de téléphone valide"
                                    }
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    name="roadName"
                                    label="Adresse de votre entreprise"
                                    id="roadName"
                                    autoComplete="road-name"
                                    onChange={handleChange}
                                    error={stockError.roadName.error}
                                    helperText={stockError.roadName.error &&
                                        "Veuillez saisir un numéro de téléphone valide"
                                    }
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    autoComplete="postal-code"
                                    name="postalCode"
                                    required
                                    fullWidth
                                    id="postalCode"
                                    label="Code Postal"
                                    autoFocus
                                    onChange={handleChange}
                                    error={stockError.postalCode.error}
                                    helperText={stockError.postalCode.error &&
                                        "Veuillez saisir un numéro de téléphone valide"
                                    }
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    required
                                    fullWidth
                                    id="city"
                                    label="Ville"
                                    name="city"
                                    autoComplete="city-name"
                                    onChange={handleChange}
                                    error={stockError.city.error}
                                    helperText={stockError.city.error &&
                                        "Veuillez saisir un numéro de téléphone valide"
                                    }
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <FormControlLabel
                                    control={<Checkbox value="allowExtraEmails" color="primary"/>}
                                    label="Je souhaite recevoir de l'inspiration, des promotions marketing et des mises à jour par e-mail."
                                />
                            </Grid>
                        </Grid>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{mt: 3, mb: 2}}
                            disabled={!readyToSubmit}
                        >
                            Inscrivez-vous
                        </Button>
                        <Grid container justifyContent="flex-end">
                            <Grid item>
                                <Link href="/connexion" variant="body2">
                                    Vous avez déjà un compte ? Connectez-vous
                                </Link>
                            </Grid>
                        </Grid>
                    </Box>
                </Box>
                <Copyright sx={{mt: 5}}/>
            </Container>
    );
}