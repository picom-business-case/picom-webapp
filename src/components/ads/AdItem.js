import React from "react";
import {Grid} from "@mui/material";
import testImg from "../../assets/ad/discover-jungle.jpg";
import Box from "@mui/material/Box";
import CalendarTodayIcon from '@mui/icons-material/CalendarToday';
import EventBusyIcon from '@mui/icons-material/EventBusy';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import Typography from "@mui/material/Typography";
import EditButtonIcon from "../button/icon/EditButtonIcon";
import PauseButtonIcon from "../button/icon/PauseButtonIcon";
import DeleteButtonIcon from "../button/icon/DeleteButtonIcon";
import Paper from "@mui/material/Paper";
import {useNavigate} from "react-router-dom";

/**
 * @param ad Ad data item
 */
const AdItem = ({ad}) => {

    const navigate = useNavigate();

    const redirectToEdit = () => {
        navigate('/compte/campagne-pub/modifier');
    }

    const deleteAdFeature = () => {

    }

    return (
        <Paper elevation={5} >


        <Grid container item xs={12} alignItems="space-between">
            <Grid item xs={12} sm={5} md={4} lg={4}>
                <img src={testImg} alt="Super" style={{maxHeight: "200px", width: "100%"}}/>
            </Grid>
            <Grid container item xs={12} sm={6} md={7} lg={7} ml={2} justifyContent="space-around" flexDirection="column">
                <Typography variant="h5" textAlign="center">
                    Campagne Dell
                </Typography>
                <Box sx={{display:" flex"}}>
                    <CalendarTodayIcon />
                    <Typography paragraph>
                        04-12-2021
                    </Typography>
                </Box>
                <Box sx={{display:" flex"}}>
                    <EventBusyIcon />
                    <Typography paragraph>
                        04-12-2021
                    </Typography>
                </Box>
                <Grid container justifyContent="space-between">
                    <Box sx={{display:" flex"}}>
                        <LocationOnIcon />
                        <Typography paragraph>
                            3
                        </Typography>
                    </Box>
                    <Box sx={{display:" flex"}}>
                        <EditButtonIcon onclick={redirectToEdit} />
                        <PauseButtonIcon />
                        <DeleteButtonIcon onclick={deleteAdFeature} />
                    </Box>
                </Grid>
            </Grid>
        </Grid>
        </Paper>
    )
}

export default AdItem;