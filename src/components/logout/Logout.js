import React, {useEffect} from "react";
import AuthenticationService from "../../services/AuthenticationService";
import {useNavigate} from "react-router-dom";


export const LogoutFeature = () => {
    const navigate = useNavigate();

    useEffect(() => {
        AuthenticationService.logout();
        navigate('/');
    })
}