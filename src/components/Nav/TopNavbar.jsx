import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { Link } from "react-scroll";
// Components
import Sidebar from "../Nav/Sidebar";
import Backdrop from "../elements/Backdrop";
// Assets
import BurgerIcon from "../../assets/svg/BurgerIcon";
import Box from "@mui/material/Box";
import {Menu, Tooltip} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import Avatar from "@mui/material/Avatar";
import MenuItem from "@mui/material/MenuItem";
import Typography from "@mui/material/Typography";
import AddBoxOutlinedIcon from "@mui/icons-material/AddBoxOutlined";
import FolderOpenOutlinedIcon from "@mui/icons-material/FolderOpenOutlined";
import AdbIcon from "@mui/icons-material/Adb";
import LogoutOutlinedIcon from "@mui/icons-material/LogoutOutlined";
import {ROUTE_ACCOUNT_AD_LIST, ROUTE_ACCOUNT_SETTINGS, ROUTE_LOGOUT} from "../../constants/routeNameConstant";
import avatarLogo from "../../assets/logo/avatar-1.png";
import picomLogo from "../../assets/logo/logoPiCom.png";
import {useNavigate} from "react-router-dom";

export default function TopNavbar({isConnected}) {
  const [y, setY] = useState(window.scrollY);
  const [sidebarOpen, toggleSidebar] = useState(false);
  const [anchorElUser, setAnchorElUser] = React.useState(null);
  const navigate = useNavigate();
  const pagesConnected = [
    {
      name:'Nouvelle annonce',
      icon: <AddBoxOutlinedIcon fontSize="large" />,
      link: '/compte/ajout-annonce'
    },
    {
      name:'Mes annonces',
      icon: <FolderOpenOutlinedIcon fontSize="large" />,
      link: '/compte/annonce'
    },
    {
      name:'Compte',
      icon: <AdbIcon fontSize="large" />,
      link: '/compte/parametres'
    },
    {
      name:'Déconnexion',
      icon: <LogoutOutlinedIcon fontSize="large" />,
      link: ROUTE_LOGOUT
    }
  ];
  const settings = [
    {
      name:'Mes annonces',
      icon: <FolderOpenOutlinedIcon fontSize="large" />,
      link: ROUTE_ACCOUNT_AD_LIST
    },
    {
      name:'Compte',
      icon: <AdbIcon fontSize="large" />,
      link: ROUTE_ACCOUNT_SETTINGS
    },
    {
      name:'Déconnexion',
      icon: <LogoutOutlinedIcon fontSize="large" />,
      link: ROUTE_LOGOUT
    }
  ];

  useEffect(() => {
    window.addEventListener("scroll", () => setY(window.scrollY));
    return () => {
      window.removeEventListener("scroll", () => setY(window.scrollY));
    };
  }, [y]);

   const handleOpenUserMenu = (event) => {
        setAnchorElUser(event.currentTarget);
    };

   const handleCloseUserMenu = () => {
       setAnchorElUser(null);
   };

  return (
    <>
      <Sidebar sidebarOpen={sidebarOpen} toggleSidebar={toggleSidebar} />
      {sidebarOpen && <Backdrop toggleSidebar={toggleSidebar} />}
      <Wrapper className="flexCenter animate whiteBg" style={y > 100 ? { height: "60px" } : { height: "80px" }}>
        <NavInner className="container flexSpaceCenter">
          <Link className="pointer flexNullCenter" to="/home" smooth={true}>
            <img src={picomLogo} alt="Picom" style={{height: "50px"}}/>

          </Link>
          <BurderWrapper className="pointer" onClick={() => toggleSidebar(!sidebarOpen)}>
            <BurgerIcon />
          </BurderWrapper>
          <UlWrapper className="flexNullCenter">
            <li className="semiBold font15 pointer">
              <Link activeClass="active" style={{ padding: "10px 15px" }} to="/#home" spy={true} smooth={true} offset={-80}>
                Home
              </Link>
            </li>
            <li className="semiBold font15 pointer">
              <Link activeClass="active" style={{ padding: "10px 15px" }} to="services" spy={true} smooth={true} offset={-80}>
                Services
              </Link>
            </li>
            <li className="semiBold font15 pointer">
              <Link activeClass="active" style={{ padding: "10px 15px" }} to="projects" spy={true} smooth={true} offset={-80}>
                Projects
              </Link>
            </li>
            <li className="semiBold font15 pointer">
              <Link activeClass="active" style={{ padding: "10px 15px" }} to="blog" spy={true} smooth={true} offset={-80}>
                Blog
              </Link>
            </li>
            <li className="semiBold font15 pointer">
              <Link activeClass="active" style={{ padding: "10px 15px" }} to="pricing" spy={true} smooth={true} offset={-80}>
                Pricing
              </Link>
            </li>
            <li className="semiBold font15 pointer">
              <Link activeClass="active" style={{ padding: "10px 15px" }} to="contact" spy={true} smooth={true} offset={-80}>
                Contact
              </Link>
            </li>
          </UlWrapper>
          { isConnected ? (
              <UlWrapperRight className="flexNullCenter">

                <li className="semiBold font15 pointer flexCenter">
                  <a href="/inscription" className="radius8 greenBg" style={{ padding: "10px 15px" }}>
                    Créer une campagne
                  </a>
                </li>
                {/*<li className="semiBold font15 pointer">
                  <a href="/connexion" style={{ padding: "10px 30px 10px 0" }}>
                    Créer une campagne
                  </a>
                </li>*/}
                  <Box sx={{ flexGrow: 0, ml: 2 }}>
                        <Tooltip title="Open settings">
                            <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                                <Avatar alt="Remy Sharp" src={avatarLogo} />
                            </IconButton>
                        </Tooltip>
                        <Menu
                            sx={{ mt: '45px' }}
                            id="menu-appbar"
                            anchorEl={anchorElUser}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={Boolean(anchorElUser)}
                            onClose={handleCloseUserMenu}
                        >
                            {settings.map((setting) => (
                                <MenuItem
                                    key={setting.name}
                                    onClick={() => {handleCloseUserMenu(); navigate(setting.link)}}
                                >
                                    <Typography textAlign="center">{setting.name}</Typography>
                                </MenuItem>
                            ))}
                        </Menu>
                    </Box>
              </UlWrapperRight>
          ): (
              <UlWrapperRight className="flexNullCenter">
                <li
                    className="semiBold font15 pointer link-custom"
                    style={{ padding: "10px 30px 10px 0" }}
                    onClick={() => navigate("/connexion")}
                >
                  {/*<a href="/connexion" >*/}
                    Connexion
                 {/* </a>*/}
                </li>
                <li
                    className="semiBold font15 pointer flexCenter radius8 orangeBg"
                    style={{ padding: "10px 15px" }}
                    onClick={() => navigate("/inscription")}
                >
                {/*  <a href="/inscription" className="radius8 greenBg" style={{ padding: "10px 15px" }}>*/}
                    Inscription
                  {/*</a>*/}
                </li>
              </UlWrapperRight>
          )}

        </NavInner>
      </Wrapper>
    </>
  );
}

const Wrapper = styled.nav`
  width: 100%;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 999;
`;
const NavInner = styled.div`
  position: relative;
  height: 100%;
`
const BurderWrapper = styled.button`
  outline: none;
  border: 0px;
  background-color: transparent;
  height: 100%;
  padding: 0 15px;
  display: none;
  @media (max-width: 760px) {
    display: block;
  }
`;
const UlWrapper = styled.ul`
  display: flex;
  @media (max-width: 760px) {
    display: none;
  }
`;
const UlWrapperRight = styled.ul`
  @media (max-width: 760px) {
    display: none;
  }
`;


