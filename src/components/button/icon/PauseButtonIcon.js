import React from "react";
import PauseIcon from '@mui/icons-material/Pause';
import theme from "../../../theme";

/**
 *
 * @param onclick method for onclick event
 */
const PauseButtonIcon = ({onclick}) => {

    return(
        <PauseIcon
            onClick={onclick}
            sx={{
                fontSize: "40px",
                '&:hover':{
                    color: theme.palette.primary.main,
                    cursor: "pointer"
                },
            }}
        />
    )

}

export default PauseButtonIcon;