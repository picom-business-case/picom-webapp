import React from "react";
import EditIcon from '@mui/icons-material/Edit';
import theme from "../../../theme";

/**
 *
 * @param onclick method for onclick event
 */
const EditButtonIcon = ({onclick}) => {

    return(
        <EditIcon
            onClick={onclick}
            sx={{
                fontSize: "40px",
                '&:hover':{
                    color: theme.palette.secondary.main,
                    cursor: "pointer"
                },
            }}
        />
    )

}

export default EditButtonIcon;