import React from "react";
import DeleteIcon from '@mui/icons-material/Delete';
import theme from "../../../theme";

/**
 *
 * @param onclick method for onclick event
 */
const DeleteButtonIcon = ({onclick}) => {

    return(
        <DeleteIcon
            onClick={onclick}
            sx={{
                fontSize: "40px",
                '&:hover':{
                    color: theme.palette.error.main,
                    cursor: "pointer"
                },
            }}
        />
    )

}

export default DeleteButtonIcon;