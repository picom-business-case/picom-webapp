import React from "react";
import styled from "styled-components";
import theme from "../../theme";

export default function FullButton({ title, action, border }) {
    return (
        <Wrapper
            className="animate pointer radius8"
            onClick={action ? () => action() : null}
            border={border}
        >
            {title}
        </Wrapper>
    );
}

const Wrapper = styled.button`
  border: 1px solid ${(props) => (props.border ? "#707070" : theme.palette.primary.main)};
  background-color: ${(props) => (props.border ? "transparent" : theme.palette.primary.main)};
  width: 100%;
  padding: 15px;
  outline: none;
  color: ${(props) => (props.border ? "#707070" : "#fff")};
  :hover {
    background-color: ${(props) => (props.border ? "transparent" : theme.palette.primary.dark)};
    border: 1px solid ${theme.palette.primary.dark};
    color: ${(props) => (props.border ? theme.palette.primary.main : "#fff")};
  }
`;

