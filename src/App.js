import './App.css';
import {BrowserRouter} from "react-router-dom";
import { Routes, Route } from "react-router-dom";
import config from "./config";
import NavbarCustomer from "./components/navbar/NavbarCustomer/NavbarCustomer";
import FooterCustomer from "./components/footer/FooterCustomer";
import Box from "@mui/material/Box";

import {isUserLoggedIn} from "./services/baseApi";
import {useEffect, useState} from "react";
import TopNavbar from "./components/Nav/TopNavbar";
import Header from "./components/Sections/Header";
import Footer from "./components/Sections/Footer";
import {useSelector} from "react-redux";
import {UserInformationsSelector} from "./selectors";

function App() {
    const user = useSelector(UserInformationsSelector);
    const [isConnected, setIsConnected] = useState(isUserLoggedIn());

    useEffect(() => {
        console.log("is Connected ?? ", isConnected);
        setIsConnected(isUserLoggedIn())
    }, [isConnected])
  return (
    <BrowserRouter className="App">
     {/* <NavbarCustomer isConnected={isConnected} />*/}
        <TopNavbar isConnected={isConnected} />

      <Box sx={{minHeight: "80vh", mt: 12}}>
          <Routes>
              {config.routes.map((route) => (
                      <Route
                          key={route.path}
                          path={route.path}
                          exact
                          element={route.component}
                      />
              ))}
                //TODO config security with redux selector
              {config.routesConnected.map((route) => (
                  <Route
                      key={route.path}
                      path={route.path}
                      exact
                      element={route.component}
                  />
              ))}

          </Routes>
      </Box>
       <FooterCustomer />
        <Footer />
    </BrowserRouter>
  );
}

export default App;
