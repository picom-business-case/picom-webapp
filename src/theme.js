import {createTheme} from "@mui/material";

const theme = createTheme({
    palette: {
        mode:"light",
        primary: {
            main: '#FF9F1C',
            contrastText: '#fff',
        },
        secondary: {
            main: '#2EC4B6',
            contrastText: '#ffffff',
        },
    },
    shape: {
        borderRadius: 4,
    },
    components: {
        MuiButton:{
            styleOverrides:{
                root:{
                    color: '#ffffff',
                    borderRadius: '4px',
                }
            }
        },
        MuiTypography: {
            styleOverrides:{
                h1: {
                    fontSize: '3.1em',
                    fontWeight: 600
                },
                h4:{
                    fontSize: '1.8em',
                    fontWeight: 'bold',
                }
            }
        }
    }
})

export default theme;