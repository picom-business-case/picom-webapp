import baseApi from "../baseApi";

/**
 * Get all Ads of an user
 * @returns
 */
const getAdsByUserAPI = () => {


    return baseApi.get(`api/user/ads`);

   /* return new Promise((resolve, reject) => {
        const request = new Request(
            `${process.env.REACT_APP_API_URL}/api/ad`,
            {
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                },/!*
                body: JSON.stringify({
                    activities
                }),*!/
            }
        );

        fetch(request)
            .then((res) => {
                res.json().then((json) => {
                    if (json.success) {
                        resolve({
                            activitiesList: json.activitiesKey,
                        });
                    } else {
                        console.error(json.error);
                        reject(json.error);
                    }
                });
            })
            .catch((err) => {
                console.error(err);
                reject(err);
            });
    });*/
};

export default getAdsByUserAPI;
