import baseApi from "../baseApi";

const registerAPI =  (data) => {

    return baseApi.post(`auth/register`, data);
}

export default registerAPI;