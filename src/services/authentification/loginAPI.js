/**
 * Get all Ads of an user
 * @param loginData form data of login form email + password
 * @returns
 */
const loginAPI = ({loginData}) => {
    return new Promise((resolve, reject) => {
        const request = new Request(
            `${process.env.REACT_APP_API_URL}/auth/login`,
            {
                method: "POST",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                },
                body: loginData,
            }
        );

        fetch(request)
            .then((res) => {
                res.json().then((json) => {
                    if (json.success) {
                        resolve({
                            user: json.user,
                        });
                    } else {
                        console.error(json.error);
                        reject(json.error);
                    }
                });
            })
            .catch((err) => {
                console.error(err);
                reject(err);
            });
    });
};

export default loginAPI;
