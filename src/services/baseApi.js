import axios from "axios";
import {USER_NAME_SESSION_ATTRIBUTE_NAME, USER_TOKEN_SESSION_ATTRIBUTE_NAME} from "./AuthenticationService";

const baseApi = axios.create({
    baseURL: `http://localhost:8280/`,
    headers: {
        Accept: "application/json",
        "Content-Type": "application/json",

    },
    "Access-Control-Allow-Origin": "*"
})
baseApi.interceptors.request.use(
    (config) => {
        if (isUserLoggedIn()) {
            console.log("config done ");
            config.headers.authorization = createJWTToken(sessionStorage.getItem(USER_TOKEN_SESSION_ATTRIBUTE_NAME));
        }
        return config;
    }
)

const createJWTToken = (token) =>{
    return 'Bearer ' + token
}

export const isUserLoggedIn = () => {
    let user = sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME)
    if (user === null) return false
    return true
}

export default baseApi;