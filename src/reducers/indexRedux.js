import { configureStore } from '@reduxjs/toolkit'
import RoleReducer from "./RoleReducer";
import UserReducer from "./UserReducer";

export default configureStore({
    reducer: {
        role: RoleReducer,
        user: UserReducer
    },
    devTools: true
})