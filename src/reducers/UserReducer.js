//import Cookies from "js-cookie";
import {
    SET_ROLE,
    SET_USER_DISCONNECTED,
    SET_USER_FAVOURITE,
    SET_USER_INFORMATIONS,
    SET_USER_PROFILE_INFORMATIONS,
    SET_USER_TOKEN,
} from "../constants/ActionTypes";
import roles from "../constants/AccountType";
import {USER_NAME_SESSION_ATTRIBUTE_NAME, USER_TOKEN_SESSION_ATTRIBUTE_NAME} from "../services/AuthenticationService";

const initialState = {
    token: sessionStorage.getItem(USER_TOKEN_SESSION_ATTRIBUTE_NAME) || "",
    data: sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME) || null,
    role: roles.CUSTOMER,
};

/*let role = Cookies.get("role") || null;
if (!role ) {
    role =
        Cookies.get("data") && JSON.parse(Cookies.get("data")).kind === AccountType.INDIVIDUAL
            ? roles.INDIVIDUAL
            : roles.PROFESSIONAL;
}
if (role) initialState.role = role;*/

const UserInformationsReducer = (state = initialState, action) => {
    switch (action.type) {
        /*case SET_USER_INFORMATIONS:
            let role = action.payload.data.kind === AccountType.INDIVIDUAL ? roles.INDIVIDUAL : roles.PROFESSIONAL;
            Cookies.set("role", role, {secure: true, sameSite: 'none'});
            action.payload.data.qualifications = [];
            Cookies.set("data", JSON.stringify(action.payload.data), {secure: true, sameSite: 'none'});
            return { ...state, data: action.payload.data, role };

        case SET_USER_PROFILE_INFORMATIONS:
            action.payload.data.qualifications = [];
            Cookies.set("data", JSON.stringify(action.payload.data), {secure: true, sameSite: 'none'});
            return { ...state, data: action.payload.data };*/

        case SET_USER_TOKEN:
            sessionStorage.setItem(USER_TOKEN_SESSION_ATTRIBUTE_NAME, action.payload.token);
            return { ...state, token: action.payload.token };

        case SET_USER_DISCONNECTED:
            sessionStorage.clear();

            return { ...state, token: "", data: null };

/*        case SET_USER_FAVOURITE:
            let data = {};
            if (action.payload.value)
                data = {
                    ...state.data,
                    favourites: [...state.data.favourites, action.payload.announcementId],
                };
            else
                data = {
                    ...state.data,
                    favourites: state.data.favourites.filter((id) => id !== action.payload.announcementId),
                };
            Cookies.set("data", JSON.stringify(data), {secure: true, sameSite: 'none'});
            return { ...state, data };
        case SET_ROLE:
            Cookies.set("role", action.payload.role, {secure: true, sameSite: 'none'});
            return { ...state, role: action.payload.role };*/
        default:
            return state;
    }
};

export default UserInformationsReducer;
